# organization
This repository provides a framework of systems that can be used to drive game events and interactions. The repository should be cloned into your Unity project Assets/ folder.

From the root of your Unity project:
```bash
cd Assets
git clone <this-repository>
```

This repository was last updated for Unity Editor Version `2019.3.15f1`

# infinity-world
## design goals
“Infinity World” is the working title of an action-driven, modular, sandbox environment. It is less of a game and more of a *framework* for interactions with and within this environment. It is the hope that any game built around this environment will provide for dynamic, emergent, and interesting interactions.

### Action-driven.
As opposed to story-driven games, where interactions are mostly scripted or pre-determined, the foundation of the “Infinity World” framework is in its interacting systems. Events arise from these interactions, allowing the player to create their own stories and adventures.

### Sandbox.
In the spirit of letting the player create their own stories and adventures, the framework creates opportunities for the player to choose how they interact with the environment. This allows for objectives to emerge from the *interactions of systems*. This means that the design is system-first, objective-later. 

### Modular.
Game systems are designed with the intent to be modular and extensible. This allows the game to grow beyond humble beginnings, expanding iteratively and with community support.

---

As a contributor, you are implementing (or modifying) systems. Within this framework, it is helpful to think of a **system** as:

- a representation of some process or mechanism
- that operates on data, within configured rules or constraints, to manage its state.

As some examples, Infinity World game systems could be "environmental" (DateTime, Weather) or "interpersonal" (Interactions, Trading) or "personal" (Health, Hunger). This is not an exhaustive list or an exhaustive classification. The intent is to explore possibilities as the list of systems grows and matures; however, in order to keep the project maintanable and functional, certain design principles must be established.

## design principles
Systems should be designed with modularity in mind. Modularity supports composability (systems are coexisting or cooperative) while discouraging coupling (systems do not rely on one another). The following principles produce a framework that is a tool rather than a constraint.

### 1. A system has a clear and unitary purpose. 
Systems that are complex or attempt to manage multiple processes are harder to maintain. They are more constrained in their utilization.

### 2. Dependencies across systems should be limited.
A system runs independently from others and requires no input for its base operation. Coupling should be limited to high-level outputs.

Changes in one system forced by changes in another should be the result of a change in the forcing system’s output, not some second-order operation. Prefer events and signals between systems rather than intimate knowledge of functions and members.

Within the structure of the project files, systems are organized into separate folders. This supports their inclusion as “modules” into a game built from the framework.

### 3. A system should not break if another system is removed.
This is just another way to state principle #2, but it bears repeating. 

If a system is so dependent on another that its base operation breaks without its inclusion, then they are by-definition coupled (and should perhaps be a single system).

For example, you might think that every system should be dependent upon DateTime. Consider that the game engine clock and the "world" clock are two separate measures of time. Animations and even interactions should continue to work even if all notion of "time" is removed from the game.

---

This list of principles may grow over time.

Each folder in this root directory represents a system. Explore their readme's to learn more.
