﻿using IW.Console;

namespace IW.AI
{
    public class SetAVCommand : TargetCommand
    {
        public override string GetCommand()
        {
            return "setav";
        }

        public override Property[] GetProperties()
        {
            return new Property[]
            {
                new Property { Name = "Property", Type = typeof(string) },
                new Property { Name = "Value", Type = typeof(float) }
            };
        }

        protected override string Logic(params object[] list)
        {
            string prop = list[0].ToString();
            float val = (float)list[1];

            string success = "Unknown error.";
            if (Target.GetComponentInChildren<Motivation.MotivatorList>() != null)
            {
                Motivation.MotivatorList motivators = Target.GetComponentInChildren<Motivation.MotivatorList>();
                Motivation.Motivator m = motivators.Get(prop);

                if (m == null)
                {
                    return string.Format("Object id {0} does not have property {1}", Target.GetInstanceID(), prop);
                }

                m.Set(val);

                success = string.Format("Object id {0} property {1} value set to {2}", Target.GetInstanceID(), prop, m.Get().Value);
            }
            else
            {
                return string.Format("Object id {0} has no container for property {1}", Target.GetInstanceID(), prop);
            }
            return success;
        }
    }
}