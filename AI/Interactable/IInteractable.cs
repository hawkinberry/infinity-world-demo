﻿using IW.AI.Actor;

namespace IW.AI.Interactable
{
    public interface IInteractable
    {
        string GetHelpText(IActor owner);

        bool Use(IActor owner);
    }
}