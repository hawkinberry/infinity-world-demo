﻿using IW.AI.Actor;
using UnityEngine;

namespace IW.AI.Interactable
{
    public class PushInteract : MonoBehaviour, IInteractable
    {
        private Rigidbody rigidbody;

        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        public string GetHelpText(IActor owner)
        {
            return "Throw";
        }

        public bool Use(IActor owner)
        {
            var ownerTransform = owner.GetTransform();
            rigidbody.AddForce((ownerTransform.forward + ownerTransform.up) * 20, ForceMode.Impulse);
            return true;
        }
    }
}