﻿using DialogSystem;
using IW.AI.Actor;
using Subtegral.DialogueSystem.DataContainers;
using UnityEngine;

namespace IW.AI.Interactable
{
    public class NPCInteract : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private DialogueContainer conversation;

        public string GetHelpText(IActor owner)
        {
            if (owner.GetActorState() == ActorState.Sneaking)
            {
                return "Pickpocket";
            }
            return "Talk";
        }

        public bool Use(IActor owner)
        {
            ///TODO: check if NPC is busy
            if (owner.GetActorState() == ActorState.Sneaking)
            {
                return true;
            }

            Transform ownerTransform = (owner as MonoBehaviour).transform;
            Vector3 targetLook = new Vector3(ownerTransform.position.x, transform.position.y, ownerTransform.position.z);
            transform.LookAt(targetLook);

            if (conversation == null)
                return false;

            DialogUI.Self.StartDialog(name, conversation);

            return true;
        }
    }
}