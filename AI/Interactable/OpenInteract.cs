﻿using IW.AI.Actor;
using UnityEngine;

namespace IW.AI.Interactable
{
    public class OpenInteract : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private bool AutoClose;
        [SerializeField]
        private float CloseTimer = 5;
        private float timer = 0;

        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void FixedUpdate()
        {
            if (AutoClose)
            {
                if (timer > 0)
                {
                    timer -= Time.fixedDeltaTime;
                    if (timer <= 0)
                    {
                        animator.SetBool("Open", false);
                    }
                }
            }
        }

        public string GetHelpText(IActor owner)
        {
            if (animator.GetBool("Open"))
            {
                return "Close";
            }
            return "Open";
        }

        public bool Use(IActor owner)
        {
            bool isOpen = animator.GetBool("Open");
            animator.SetBool("Open", !isOpen);
            if (AutoClose)
            {
                if (!isOpen)
                {
                    // just opened
                    timer = CloseTimer;
                }
            }
            return true;
        }
    }
}