﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IW.AI.POI
{
    public class POIContainer : MonoBehaviour
    {
        protected List<IPointOfInterest> pois = new List<IPointOfInterest>();

        public void AddPOI(IPointOfInterest poi)
        {
            pois.Add(poi);
        }

        public void RemovePOI(IPointOfInterest poi)
        {
            pois.Remove(poi);
        }
    }
}