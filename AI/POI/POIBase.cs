﻿using UnityEngine;

namespace IW.AI.POI
{
    public class POIBase : MonoBehaviour, IPointOfInterest
    {
        private POIContainer container = null;

        private void OnEnable()
        {
            if (container == null)
            {
                container = GetComponentInParent<POIContainer>();
            }
            container.AddPOI(this);
        }

        private void OnDisable()
        {
            container.RemovePOI(this);
        }
    }
}