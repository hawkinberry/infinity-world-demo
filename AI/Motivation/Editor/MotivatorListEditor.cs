﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Reflection;

/***************************************************
 * Implemented from Unite Europe 2016 demo
 * https://youtu.be/9bHzTDIJX_Q?t=1662
 * 
 * It includes a demonstration of creating a custom
 * Editor for a list. It also includes a custom
 * Editor for a ReordableList.
***************************************************/
namespace IW.AI.Motivation
{
    // See timestamp 31:25
    public class EditorGUIHelper
    {
        static MethodInfo boldFontMethodInfo = null;
        public static void SetBoldDefaultFont(bool value)
        {
            if (boldFontMethodInfo == null)
            {
                boldFontMethodInfo = typeof(EditorGUIUtility).GetMethod("SetBoldDefaultFont", BindingFlags.Static | BindingFlags.NonPublic);
            }

            boldFontMethodInfo.Invoke(null, new[] { value as object });
        }
    }
    
    // See timestamp 27:40
    [CustomEditor(typeof(MotivatorList))]
    public class MotivatorListEditor : Editor
    {
        MotivatorList listTarget;
        SerializedProperty motivations;

        GUIStyle dragBoxStyle = new GUIStyle();

        Vector2 scrollPosition;

        public void OnEnable()
        {
            //dragBoxStyle.stretchWidth = true;
            dragBoxStyle.alignment = TextAnchor.MiddleCenter;
        }

        public override void OnInspectorGUI()
        {
            listTarget = (MotivatorList)target;
            motivations = serializedObject.FindProperty("motivations");

            GUILayout.BeginVertical();
            {
                DropAreaGUI();

                DrawClearListButton();

                scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false);
                {
                    DrawStatesInspector();
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }

        // https://gist.github.com/bzgeb/3800350
        public void DropAreaGUI()
        {
            Event evt = Event.current;
            Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, /*GUILayout.ExpandWidth(true),*/ dragBoxStyle);
            GUI.Box(drop_area, "Drag Motivator here");

            switch (evt.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!drop_area.Contains(evt.mousePosition))
                        return;

                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (evt.type == EventType.DragPerform)
                    {
                        DragAndDrop.AcceptDrag();

                        foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
                        {
                            // Do On Drag Stuff here
                            if (false == (dragged_object is Motivator))
                            {
                                Debug.Log("List does not accept type " + dragged_object.GetType());
                                continue;
                            }

                            Motivator newMotivator = (Motivator)dragged_object;
                            //Motivator clone = (Motivator)dragged_object;
                            //Motivator newMotivator = Motivator.Instantiate(clone);
                            //newMotivator.name = clone.name; // change base object name. Can we override class Instantiate to do this?

                            if (false == listTarget.Add(newMotivator))
                            {
                                Debug.Log("List already contains Motivator " + newMotivator.Name);
                            }
                            else
                            {
                                Undo.RecordObject(listTarget, "Add Motivator");
                                EditorUtility.SetDirty(listTarget);
                            }
                        }
                    }
                    break;
            }


        }


        void DrawStatesInspector()
        {
            GUILayout.Space(5);
            GUILayout.Label("Motivators", EditorStyles.boldLabel);
   
            if (listTarget.GetCount() == 0)
            {
                GUILayout.Label("Empty");
                return;
            }

            for (int i = 0; i < listTarget.GetCount(); i++)
            {
                DrawState(i);
            }
        }

        void DrawState(int index)
        {
            if (false == listTarget.Has(index))
            {
                Debug.Log("Target does not contain index " + index + ".");
                return;
            }

            Motivator thisItem = listTarget.Get(index);

            GUILayout.BeginVertical();
            {
                GUILayout.BeginHorizontal();
                {
                    // BeginChangeCheck() is a useful way to see if an inspector variable was changed
                    EditorGUI.BeginChangeCheck();

                    GUILayout.Label(thisItem.Name, GUILayout.MinWidth(100));
                    //EditorGUILayout.ObjectField(index.ToString() + ":", thisItem, thisItem.GetType(), false);

                    EditorGUILayout.ObjectField(thisItem, thisItem.GetType(), false);

                    GUILayout.FlexibleSpace();

                    // If a variable was modified, EndChangeCheck() returns true so we can perform
                    // necessary behavior like storying the updates
                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(listTarget, "Modify State");

                        // Whenever you modify a component variable directly without using serializedObject,
                        // you need to tell Unity that this component has changed so that the values are saved
                        // next time the user saves the project
                        EditorUtility.SetDirty(listTarget);
                    }

                    if (GUILayout.Button("X", GUILayout.Width(20)))
                    {
                        // DisplayDialog is a very useful method to create a simple popup check. You can use this
                        // to simply display information to the user that something has changed, which requires
                        // user confirmation.
                        string confirm = "Are you sure you want to remove the Motivator ?";
                        if (true == EditorUtility.DisplayDialog("Confirm Delete", confirm, "Yes", "No"))
                        {
                            Undo.RecordObject(listTarget, "Delete Motivator");
                            listTarget.Remove(index);
                            EditorUtility.SetDirty(listTarget);
                        }

                    }

                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }

        public void DrawClearListButton()
        {
            if (GUILayout.Button("Clear List"))
            {
                // DisplayDialog is a very useful method to create a simple popup check. You can use this
                // to simply display information to the user that something has changed, which requires
                // user confirmation.
                string confirm = "Are you sure you want to clear all Motivators?";
                if (true == EditorUtility.DisplayDialog("Confirm Clear", confirm, "Yes", "No"))
                {
                    Undo.RecordObject(listTarget, "Clear Motivator List");
                    listTarget.Motivations.Clear();
                    EditorUtility.SetDirty(listTarget);
                }

            }
        }

    }
}
