﻿using System;
using UnityEngine;
using UnityEditor;

namespace IW.AI.Motivation
{
    [CustomEditor(typeof(TestThirst))]
    public class TestThirstEditor : Editor
    {
        TestThirst test;
        float testValue = 10;

        public override void OnInspectorGUI()
        {
            test = (TestThirst)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Add:", GUILayout.Width(50));
                string value;
                value = GUILayout.TextField(testValue.ToString());

                try
                {
                    testValue = float.Parse(value);
                }
                catch (Exception e)
                {
                    Debug.Log("Must enter numerical value.");
                }


                if (GUILayout.Button("Replenish"))
                {
                    test.Replenish(testValue);
                }
            }
            GUILayout.EndHorizontal();
        }
    }
}