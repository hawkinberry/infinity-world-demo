﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IW.AI.Motivation
{
    public class TestThirst : MonoBehaviour
    {
        public Motivator thirst;

        public void Replenish(float val)
        {
            thirst.Replenish(val);
        }
    }
}