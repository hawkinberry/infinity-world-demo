﻿using System;
using UnityEngine;

namespace IW.AI.Motivation
{
    using StateType = System.Single;

    /** This type of Motivator is a static value. 
    *   Its State can be replenished or reduced by 
    *   an input amount, or it can be set explicitly.
    */
    [Serializable]
    [CreateAssetMenu(fileName = "New Motivator", menuName = "Motivator/Static Motivator")]
    public class StaticMotivator : Motivator
    {
        //! Sets the state of Motivator
        //!  param newVal The value to set
        //!  return new state value
        public virtual StateType SetState(StateType newVal)
        {
            // Because we cannot set the value of State directly (due to min/max restrictions),
            // we calculate the difference and allow State to adjust itself,
            // according to its own rules.
            StateType diff = state.Value - newVal;
            // We can call Add with either a positive or negative value:
            // the implementation implicitly performs the correct implementation.
            state.Add(diff);

            return state.Value;
        }
    }
}