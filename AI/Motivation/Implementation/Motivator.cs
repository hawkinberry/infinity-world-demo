﻿using System;
using UnityEngine;

namespace IW.AI.Motivation
{
    using StateType = System.Single;

    [Serializable]
    public class State
    {
        private StateType currentVal = 1; // The current value of this state
        [SerializeField] private StateType maxValue = 1; // The maximum value of this state
        [SerializeField] private StateType minValue = 0; // The minimum value of this state
        [SerializeField] protected StateType startVal = 1; // The initial value of this state

        public bool Initialize()
        {
            this.Value = this.StartValue;
            return true;
        }

        // ---- ACCESSORS ---- //
        public StateType Value
        {
            // Any class can get
            get { return currentVal; }
            // Only self and derived classes can set
            set { currentVal = value; }
        }

        public StateType MaxValue
        {
            // Any class can get
            get { return maxValue; }
            // Only self and derived classes can set
            set { maxValue = value; }
        }

        public StateType MinValue
        {
            // Any class can get
            get { return minValue; }
            // Only self and derived classes can set
            set { minValue = value; }
        }

        public StateType StartValue
        {
            // Any class can get
            get { return startVal; }
            // Only self and derived classes can set
            set { startVal = value; }
        }

        // ---- MODIFIERS ---- //
        // Subtract a value from the state, stopping at the minimum value
        // A negative input value will invoke Add
        public virtual void Subtract(StateType val)
        {
            // If the value is negative, we actually want to use the Add method
            if (val < 0)
            {
                this.Add(-1 * val);
            }
            else
            {
                this.Value = (val > this.Value ? this.MinValue : this.Value - val);
            }
        }

        // Add a value to the state, stopping at the maximum value
        // A negative input value will invoke Subtract
        public virtual void Add(StateType val)
        {
            // If the value is negative, we actually want to use the Subtract method
            if (val < 0)
            {
                this.Subtract(-1 * val);
            }
            else
            {
                // Note: this doesn't stop us from overruning the size of a StateType
                //       if val is very large
                StateType temp = this.Value + val;
                this.Value = (temp > this.MaxValue ? this.MaxValue : temp);
            }
        }

        public float GetRatio()
        {
            return (float)this.Value / this.MaxValue;
        }
    }

    public interface IMotivator
    {
        //! Advances the state of Motivator
        //!  param delta Time step
        //!  return new state value
        StateType Advance(float delta);
    }

    [Serializable]
    public class Motivator : ScriptableObject, IMotivator
    {
        [SerializeField] protected State state;
        [SerializeField] protected string motivatorName;
        [SerializeField] protected Priority priority;

        public static Type GetStateType()
        {
            return typeof(StateType);
        }

        // ---- ACCESSORS ---- //
        public Priority Priority
        {
            // Any class can get
            get { return priority; }
        }

        public int PriorityInt
        {
            get { return (int)priority; }
        }

        public string Name
        {
            get { return motivatorName; }
            set { motivatorName = value; }
        }

        public bool Initialize()
        {
            return state.Initialize();
        }

        public State Get()
        {
            return state;
        }

        public void Set(StateType val)
        {
            StateType newVal = state.MaxValue;

            if (val < state.MaxValue)
            {
                if (val > state.MinValue)
                {
                    newVal = val;
                }
                else
                {
                    newVal = state.MinValue;
                }
            }

            state.Value = newVal;
        }

        // ---- UTILITY ---- //
        public override string ToString()
        {
            return this.Name;
        }
        public virtual float GetUrgency()
        {
            return (1 - state.GetRatio());
        }

        public bool Compare(Priority otherPriority)
        {
            return (this.Priority <= otherPriority);
        }

        // ---- SCRIPTABLE ---- //
        public void Reset()
        {
            // Set default motivator name to object name
            this.Name = this.name;
            this.state.Initialize();
        }

        // ---- MODIFIERS ---- //
        //! Advances the state of Motivator
        //!  param delta Time step
        //!  return new state value
        public virtual StateType Advance(float delta)
        {
            return state.Value;
        }

        //! Replenishes the state of Motivator
        //!  param val The amount to replenish
        //!  return new state value
        public virtual StateType Replenish(StateType val)
        {
            state.Add(val);

            return state.Value;
        }

        //! Reduces the state of Motivator
        //!  param val The amount to reduce
        //!  return new state value
        public virtual StateType Reduce(StateType val)
        {
            state.Subtract(val);

            return state.Value;
        }
    }
}