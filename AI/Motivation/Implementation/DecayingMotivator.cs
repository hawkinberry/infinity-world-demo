﻿using System;
using UnityEngine;

namespace IW.AI.Motivation
{
    using StateType = System.Single;

    /** This type of Motivator decays over time 
    *   At a rate defined by Decay.
    *   Rate can be offset by setting Regen rate.
    */
    [Serializable]
    [CreateAssetMenu(fileName = "New Motivator", menuName = "Motivator/Decaying Motivator")]
    public class DecayingMotivator : Motivator
    {
        [SerializeField] protected StateType decay; // Measured in loss per second
        [SerializeField] protected StateType regen; // Measured in gain per second

        // ---- ACCESSORS ---- //
        public StateType Decay
        {
            // Any class can get
            get { return decay; }
            // Only self and derived classes can set
            protected set { decay = value; }
        }

        public StateType Regen
        {
            // Any class can get
            get { return regen; }
            // Only self and derived classes can set
            protected set { regen = value; }
        }

        // ---- MODIFIERS ---- //
        //! Advances the state of Motivator
        //!  param delta Time step
        //!  return new state value
        public override StateType Advance(float delta)
        {
            StateType redux = (this.Decay - this.Regen) * (StateType)delta;

            // We can call Reduce with either a positive or negative value
            // The implementation implicitly performs the correct implementation
            this.Reduce(redux);

            return state.Value;
        }

    }
}