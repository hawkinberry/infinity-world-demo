﻿namespace IW.AI.Motivation
{
    /** The following hierarchy of priorities is derived from Maslow's Hierarchy of Needs.
    *   In this implementation, a priority value of 1 is considered the highest   
    *   For INVALID = 0 is defined to conform to the preceding assertion.
    *
    *   The following are provided with example motivators.
    *
    *   A thought to consider: how does the one-dimensional representation of priority
    *   below account for humane actions? For example, a father goes hungry in order
    *   to feed his child. Or a woman sacrificing her life to save another's?
    *   It is almost as if there needs to be one level above PHYSIOLOGICAL, one
    *   that would drive the survival of the species. I will call this PRESERVATIVE.
    *   Such a comparison would only be necessary in times of hardship.
    *   
    *   There is another consideration: what of greed? The motivator would be something
    *   like Money or Power, which I would classify as SECURITY. The solution brings 
    *   another question of how we represent personalities. One individual might be
    *   more susceptible to greed than another, such that their personal SECURITY or
    *   PHYSIOLOGICAL needs outweigh the PRESERVATIVE ones.
    */
    public enum Priority
    {
        INVALID = 0, // should never be used
        PRESERVATIVE, //e.g., selfless acts that will preserve the species
        PHYSIOLOGICAL, //e.g., hunger, thirst, warmth, breath, rest
        SECURITY, //e.g., shelter, employment
        COMMUNITY, //e.g., fellowship, friendship, love
        ESTEEM, //e.g., pride, respect, confidence
        ACTUALIZATION, //e.g., purpose, creativity, leisure, freedom
        NULL_PRIORITY = 10000 // represents the lowest priority imaginable
    }
}