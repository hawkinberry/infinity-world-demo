﻿namespace IW.AI.Actor
{
    public enum ActorState
    {
        Neutral,
        Sneaking,
        Jumping
    }
}