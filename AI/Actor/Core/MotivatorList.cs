﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IW.AI.Motivation
{
    // SortedList<TKey, TValue> ? Enforces that every Motivator is uniquely named
    // BlockingCollection<T> ? Thread-safe
    // ConcurrentDictionary<TKey, TValue> ? Enforces uniqueness and is thread-safe, but
    //                                      do we really want something that blocks
    //                                      or provides try/catch function?
   [Serializable]
    public class MotivatorList : MonoBehaviour
    {
        [SerializeField]
        private List<Motivator> motivations = new List<Motivator>();
        
        public List<Motivator> Motivations
        {
            get { return motivations; }
        }

        public bool Initialize()
        {
            // For some reason, the MotivatorList inside the custom editor
            // begins with a null element
            //motivations = new List<Motivator>();
            return true;
        }

        // ---- MONOBEHAVIOUR ---- //
        public void Awake()
        {
            Debug.Log("Hello MotivatorList!");
            //foreach (Motivator m in motivations)
            //{
            //    m = Motivator.Instantiate(m);
            //}
            motivations.ForEach(m => m.Initialize());
        }

        public void FixedUpdate()
        {
            motivations.ForEach(m => m.Advance(Time.deltaTime));
        }

        // ---- UTILITY ---- //
        public int GetCount()
        {
            return motivations.Count;
        }

        public override string ToString()
        {
            string output = "Motivators: ";
            if (GetCount() == 0)
            {
                output += "EMPTY";
            }
            else
            {
                foreach (Motivator m in motivations)
                {
                    if (m == null) { continue; }
                    output += m.ToString() + " ";
                }
            }

            return output;
        }

        // ---- EXISTENCE ---- //
        public bool Has(string aName)
        {
            if (GetCount() == 0)
            {
                return false;
            }

            return motivations.Exists(m => m.Name.ToLower() == aName.ToLower());
        }

        public bool Has(int idx)
        {
            return (idx >= 0 && idx < this.GetCount());
        }

        // ---- ACCESSORS ---- //
        //! Returns Motivator by idx.
        //! Returns null if idx out of bounds.
        public Motivator Get(int idx)
        {
          if (this.Has(idx))
            {
                return motivations[idx];
            }
            else
            {
                return null;
            }
        }

        //! Returns Motivator by name.
        //! Returns null if not found.
        public Motivator Get(string aName)
        {
            if (this.Has(aName))
            {
                return motivations.Find(m => m.Name.ToLower() == aName.ToLower());
            }
            else
            {
                return null;
            }
        }

        // ---- MODIFIERS ---- //
        //! Returns indication of successful add.
        //! Note: Motivators in list must be unique by name.
        public bool Add(Motivator motivator)
        {
            // Motivators must be unique by name
            if (true == this.Has(motivator.Name))
            {
                return false;
            }

            motivations.Add(motivator);
            return true;
        }

        //! Returns indication of successful remove.
        public bool Remove(Motivator motivator)
        {
            // Motivators must be unique by name
            if (false == this.Has(motivator.Name))
            {
                return false;
            }

            motivations.Remove(motivator);
            return true;
        }

        //! Returns indication of successful remove.
        public bool Remove(int index)
        {
            if (false == this.Has(index))
            {
                return false;
            }

            Motivator target = this.Get(index);
            motivations.Remove(target);
            return true;
        }

        //! Returns indication of successful remove.
        public bool Remove(string aName)
        {
            if (false == this.Has(aName))
            {
                return false;
            }

            Motivator target = this.Get(aName);
            motivations.Remove(target);
            return true;
        }
    }
}