﻿using UnityEngine;

namespace IW.AI.Actor
{
    public interface IActor
    {
        Transform GetTransform();

        ActorState GetActorState();
    }
}