# System: Motivators (Need-driven Behaviors)

---

This page is currently a design doc. As the system matures, it can be archived and converted into an implementation guide.

---

An artificial intelligence agent executes behaviors. A simple example of a behavior might be "walking". A more complex behavior might be "searching". Behaviors chosen at random or on a scripted cycle will appear stale or lifeless to an observer. Instead, tying behaviors to **motivations** that are intuitive to the observer leads to more immersive behavior patterns.
 
As an example, imagine a *Thirst* motivator. As soon as Thirst drops, the chance to trigger a "Search for Water" behavior increases. In this design, motivators can even be ranked, perhaps on two axes: overall priority and immediate priority (or "urgency").

## Motivators
A **Motivator** is a class that manages how time affects its state. It implements an interface that is invokable in order to "advance" its state, according to its own internal rule.

```csharp
public interface IMotivator
{
	//! Advances the state of Motivator
	//!  param delta Time step
	//!  return new state value
	StateType Advance(float delta);
}
```

### Priority
The following hierarchy of priorities is derived from Maslow's Hierarchy of Needs. In this implementation, a priority value of 1 is considered the highest. For INVALID = 0 is defined to conform to the preceding assertion.

```csharp
public enum Priority
{
	INVALID = 0,
	PRESERVATIVE, //e.g., selfless acts that will preserve the species
	PHYSIOLOGICAL, //e.g., hunger, thirst, warmth, breath, rest
	SECURITY, //e.g., shelter, employment
	COMMUNITY, //e.g., fellowship, friendship, love
	ESTEEM, //e.g., pride, respect, confidence
	ACTUALIZATION //e.g., purpose, creativity, leisure, freedom
}
```
### Urgency
Urgency is a measure of a motivator's depletion. The lower the state, the greater the urgency, and the urgency will increase over time until the state is restored. 

This measure is independent from priority. One could have urgently low confidence, but if they are also dehydrated, then their self-esteem will have to wait.

### Design Guidance: Motivator Interrelation
It is possible that some motivators can be mathematically but distantly interrelated. Interrelation should be indirect to maintain decoupling between motivators. 

For example, in service of a motivator involved with the security of oneself, physical effort may cause a physiological motivator (like Hunger or Thirst) to decrease more quickly. However, this is not something that such a motivator should know directly--what if the game doesn't need a concept of hunger? 
	
Instead, the state of a motivator could be affected by core attributes of the host's state (e.g., health, exertion). These attributes will be handled independently by an appropriate system, and although their existence could be defined as an interface (to establish a baseline), the state of a motivator should not rely on their implementation (that is, Thirst should always decrease at a baseline rate).

The balance of each Motivator's decline may be a point of great tuning to avoid an unstable state machine.

## Motivator List
Each agent will have an associated **Motivator List**. Because each Motivator implements an interface, the List does not need to have any knowledge of what the Motivator really is.

The Motivator List is responsible for invoking the update of each motivator. In this case, the List is aware of the concept of time (running within the game engine's event loop) whereas each Motivator is only responsible for how time impacts its state. This way, Motivators are updated synchronously, and should the mechanism for update change, it is only changed in one place (the Motivator List).

```csharp
public void FixedUpdate()
{
	motivations.ForEach(m => m.Advance(Time.deltaTime));
}
```

## Motivation Evaluator
The agent has a **Motivation Evaluator** that operates on the Motivation List. The Evaluator will rank the current state of each Motivator based on its priority and urgency. For example,

- *Thirst* is at 60% and *Fun* is at 20%. Even though *Fun* is more urgent based on its state, *Thirst* is the higher priority and will be the stronger motivation.
- *Thirst* is at 60% and *Rest* is at 40%. Both are physiological needs. There are times when *Thirst* will override the need for Rest; but, if *Rest* is at 0%, then *Thirst* will need to wait.

The Motivation Evaluator outputs the Motivator of the greatest need.

## Behavior Engine
The **Behavior Engine** selects the appropriate behavior to fill the greatest need. This will follow a Goal Oriented Action Planning (GOAP) pattern, in order to avoid defining states and "allowable transitions" that require knowledge of the state space (this is not very modular). Instead, fulfilling the motivation (the need) becomes the **goal** while the behavior (how the need is fulfilled) is the **action**.

Once an action is selected, an **Actor** is responsible for executing the behavior (to-do).

### Mapping Needs to Behaviors
Note that this design does not assume a one-to-one correlation (although every motivator should have at least one implemented way to fulfill it). Multiple behaviors capable of fulfilling the motivator could be considered by the Behavior Engine, assuming they have some other discriminator. Behaviors could be tagged as fulfilling certain needs, thus identifying them as candidates.
