﻿Shader "Unlit/CloudShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Scale("Scale", Range(1, 100)) = 1
		_TimeScale("Time Scale", Range(0, 100)) = 1
		_Weight("Cloud Weight", Range(0, 1)) = 0.5
		_CloudFill("Cloud Fill", Range(0, 1)) = 0
		_CloudColor("Cloud Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			uniform half _Scale;
			uniform half _TimeScale;
			uniform half _Weight;
			uniform half _CloudFill;
			fixed4 _CloudColor;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float2 viewVector : TEXCOORD1;
                float4 vertex : SV_POSITION;
            };

			v2f vert(appdata v)
			{
				v2f output;
				output.vertex = UnityObjectToClipPos(v.vertex);
				output.uv = v.uv;
				float3 viewVector = mul(unity_CameraInvProjection, float4(v.uv * 2 - 1, 0, -1));
				output.viewVector = mul(unity_CameraToWorld, float4(viewVector, 0));
				return output;
			}

            sampler2D _MainTex;
			float timeScale;

			float4 mod289(float4 x)
			{
				return x - floor(x * (1.0 / 289.0)) * 289.0;
			}

			float3 mod289(float3 x)
			{
				return x - floor(x * (1.0 / 289.0)) * 289.0;
			}

			float2 mod289(float2 x)
			{
				return x - floor(x * (1.0 / 289.0)) * 289.0;
			}

			float4 permute(float4 x)
			{
				return mod289(((x * 34.0) + 1.0) * x);
			}

			float3 permute(float3 x)
			{
				return mod289((x * 34.0 + 1.0) * x);
			}

			float4 taylorInvSqrt(float4 r)
			{
				return 1.79284291400159 - 0.85373472095314 * r;
			}

			float3 taylorInvSqrt(float3 r)
			{
				return 1.79284291400159 - 0.85373472095314 * r;
			}

			float2 fade(float2 t) {
				return t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
			}

			// Classic Perlin noise
			float cnoise(float2 P)
			{
				float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
				float4 Pf = frac(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
				Pi = mod289(Pi); // To avoid truncation effects in permutation
				float4 ix = Pi.xzxz;
				float4 iy = Pi.yyww;
				float4 fx = Pf.xzxz;
				float4 fy = Pf.yyww;

				float4 i = permute(permute(ix) + iy);

				float4 gx = frac(i * (1.0 / 41.0)) * 2.0 - 1.0;
				float4 gy = abs(gx) - 0.5;
				float4 tx = floor(gx + 0.5);
				gx = gx - tx;

				float2 g00 = float2(gx.x, gy.x);
				float2 g10 = float2(gx.y, gy.y);
				float2 g01 = float2(gx.z, gy.z);
				float2 g11 = float2(gx.w, gy.w);

				float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
				g00 *= norm.x;
				g01 *= norm.y;
				g10 *= norm.z;
				g11 *= norm.w;

				float n00 = dot(g00, float2(fx.x, fy.x));
				float n10 = dot(g10, float2(fx.y, fy.y));
				float n01 = dot(g01, float2(fx.z, fy.z));
				float n11 = dot(g11, float2(fx.w, fy.w));

				float2 fade_xy = fade(Pf.xy);
				float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
				float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
				return 2.3 * n_xy;
			}

			// Classic Perlin noise, periodic variant
			float pnoise(float2 P, float2 rep)
			{
				float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
				float4 Pf = frac(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
				Pi = fmod(Pi, rep.xyxy); // To create noise with explicit period
				Pi = mod289(Pi);        // To avoid truncation effects in permutation
				float4 ix = Pi.xzxz;
				float4 iy = Pi.yyww;
				float4 fx = Pf.xzxz;
				float4 fy = Pf.yyww;

				float4 i = permute(permute(ix) + iy);

				float4 gx = frac(i * (1.0 / 41.0)) * 2.0 - 1.0;
				float4 gy = abs(gx) - 0.5;
				float4 tx = floor(gx + 0.5);
				gx = gx - tx;

				float2 g00 = float2(gx.x, gy.x);
				float2 g10 = float2(gx.y, gy.y);
				float2 g01 = float2(gx.z, gy.z);
				float2 g11 = float2(gx.w, gy.w);

				float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
				g00 *= norm.x;
				g01 *= norm.y;
				g10 *= norm.z;
				g11 *= norm.w;

				float n00 = dot(g00, float2(fx.x, fy.x));
				float n10 = dot(g10, float2(fx.y, fy.y));
				float n01 = dot(g01, float2(fx.z, fy.z));
				float n11 = dot(g11, float2(fx.w, fy.w));

				float2 fade_xy = fade(Pf.xy);
				float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
				float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
				return 2.3 * n_xy;
			}

			// Simplex Noise
			float snoise(float2 v)
			{
				const float4 C = float4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
					0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
					-0.577350269189626,  // -1.0 + 2.0 * C.x
					0.024390243902439); // 1.0 / 41.0

// First corner
				float2 i = floor(v + dot(v, C.yy));
				float2 x0 = v - i + dot(i, C.xx);

				// Other corners
				float2 i1;
				i1.x = step(x0.y, x0.x);
				i1.y = 1.0 - i1.x;

				// x1 = x0 - i1  + 1.0 * C.xx;
				// x2 = x0 - 1.0 + 2.0 * C.xx;
				float2 x1 = x0 + C.xx - i1;
				float2 x2 = x0 + C.zz;

				// Permutations
				i = mod289(i); // Avoid truncation effects in permutation
				float3 p =
					permute(permute(i.y + float3(0.0, i1.y, 1.0))
						+ i.x + float3(0.0, i1.x, 1.0));

				float3 m = max(0.5 - float3(dot(x0, x0), dot(x1, x1), dot(x2, x2)), 0.0);
				m = m * m;
				m = m * m;

				// Gradients: 41 points uniformly over a line, mapped onto a diamond.
				// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
				float3 x = 2.0 * frac(p * C.www) - 1.0;
				float3 h = abs(x) - 0.5;
				float3 ox = floor(x + 0.5);
				float3 a0 = x - ox;

				// Normalise gradients implicitly by scaling m
				m *= taylorInvSqrt(a0 * a0 + h * h);

				// Compute final noise value at P
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.y = a0.y * x1.x + h.y * x1.y;
				g.z = a0.z * x2.x + h.z * x2.y;
				return 130.0 * dot(m, g);
			}

			// Simplex Noise Grad
			float2 snoise_grad(float2 v)
			{
				const float4 C = float4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
					0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
					-0.577350269189626,  // -1.0 + 2.0 * C.x
					0.024390243902439); // 1.0 / 41.0

// First corner
				float2 i = floor(v + dot(v, C.yy));
				float2 x0 = v - i + dot(i, C.xx);

				// Other corners
				float2 i1;
				i1.x = step(x0.y, x0.x);
				i1.y = 1.0 - i1.x;

				// x1 = x0 - i1  + 1.0 * C.xx;
				// x2 = x0 - 1.0 + 2.0 * C.xx;
				float2 x1 = x0 + C.xx - i1;
				float2 x2 = x0 + C.zz;

				// Permutations
				i = mod289(i); // Avoid truncation effects in permutation
				float3 p =
					permute(permute(i.y + float3(0.0, i1.y, 1.0))
						+ i.x + float3(0.0, i1.x, 1.0));

				float3 m = max(0.5 - float3(dot(x0, x0), dot(x1, x1), dot(x2, x2)), 0.0);
				float3 m2 = m * m;
				float3 m3 = m2 * m;
				float3 m4 = m2 * m2;

				// Gradients: 41 points uniformly over a line, mapped onto a diamond.
				// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
				float3 x = 2.0 * frac(p * C.www) - 1.0;
				float3 h = abs(x) - 0.5;
				float3 ox = floor(x + 0.5);
				float3 a0 = x - ox;

				// Normalise gradients
				float3 norm = taylorInvSqrt(a0 * a0 + h * h);
				float2 g0 = float2(a0.x, h.x) * norm.x;
				float2 g1 = float2(a0.y, h.y) * norm.y;
				float2 g2 = float2(a0.z, h.z) * norm.z;

				// Compute gradient of noise function at P
				float2 grad =
					-6.0 * m3.x * x0 * dot(x0, g0) + m4.x * g0 +
					-6.0 * m3.y * x1 * dot(x1, g1) + m4.y * g1 +
					-6.0 * m3.z * x2 * dot(x2, g2) + m4.z * g2;
				return 130.0 * grad;
			}

			float4 frag(v2f input) : SV_Target
			{
				float2 uv = input.uv;

				float o = 0.5;
				float w = _Weight; // initial weight
				float s = _Scale; // scale?

				float t = _Time.x;

				for (int i = 0; i < 6; i++)
				{
					float2 coord = uv * s + float2(t * _TimeScale, 0);
					float2 period = s * 2.0;

					o += pnoise(coord, period) * w;

					s *= 2.0;
					w *= 0.5;
				}

				o = clamp(o, 0, 1);
				float3 backgroundCol = tex2D(_MainTex, uv);
				float3 col = lerp(backgroundCol, _CloudColor, o);
                return float4(col, 1);
            }
            ENDCG
        }
    }
}