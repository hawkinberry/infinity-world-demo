﻿using UnityEngine;
using UnityEngine.Rendering;

namespace IW.Skytime
{
    public class SkyComponent : MonoBehaviour
    {
        public Material Skybox;

        public float Latitude;

        public CelestialBody Sun;
        public CelestialBody Moon;

        public Light Ambient;
        public AnimationCurve AmbientIntensityCurve;

        private float ambientIntensity;

        private void Awake()
        {
            Datetime.OnDayTime += DayTime;
            Datetime.OnNightTime += NightTime;
            Datetime.OnDayChange += DayChange;

            ambientIntensity = Ambient.intensity;

            Sun.LocalLatitude = Latitude;
            Moon.LocalLatitude = Latitude;
        }

        void Update()
        {
            SetTimeOfDay(Datetime.PercentOfDay());
        }

        private void SetTimeOfDay(float percentOfDay)
        {
            Ambient.intensity = AmbientIntensityCurve.Evaluate(percentOfDay) * ambientIntensity;
        }

        private void DayChange()
        {

        }

        private void DayTime()
        {

        }

        private void NightTime()
        {

        }
    }
}