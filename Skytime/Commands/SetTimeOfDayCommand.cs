﻿using IW.Console;

namespace IW.Skytime
{
    public class SetTimeOfDayCommand : Command
    {
        public override string GetCommand()
        {
            return "settimeofday";
        }

        public override Property[] GetProperties()
        {
            return new Property[]
            {
                new Property { Name = "Hour", Type = typeof(int) }
            };
        }

        protected override string Logic(params object[] list)
        {
            int fullHour = (int)list[0];

            int hour = 0;
            int minutes = 0;

            if (fullHour > 99)
            {
                hour = fullHour / 100;
                minutes = fullHour % 100;
            }
            else
            {
                hour = fullHour;
                minutes = 0;
            }

            if (hour > 24 || hour < 0 || minutes > 59 || minutes < 0)
            {
                return "Invalid time parameter";
            }

            Datetime.Hour = hour;
            Datetime.Minutes = minutes;
            Datetime.ModifyMinutes(0);

            return string.Format("New date and time: {0}", Datetime.FullDatetimeString());
        }

        public override string ToString()
        {
            return string.Format("{0} - Parameter is in military time. Does not move forward past the current day.", base.ToString());
        }
    }
}