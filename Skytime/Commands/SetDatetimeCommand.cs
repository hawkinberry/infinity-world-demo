﻿using IW.Console;

namespace IW.Skytime
{
    public class SetDatetimeCommand : Command
    {
        public override string GetCommand()
        {
            return "setdatetime";
        }

        public override Property[] GetProperties()
        {
            return new Property[]
            {
                new Property { Name = "Day", Type = typeof(int) },
                new Property { Name = "Month", Type = typeof(int) },
                new Property { Name = "Year", Type = typeof(int), Optional = true },
                new Property { Name = "Hour", Type = typeof(int), Optional = true }
            };
        }

        protected override string Logic(params object[] list)
        {
            int day = (int)list[0];
            int month = (int)list[1];
            int year = Datetime.Year;
            int fullHour = Datetime.Hour;

            if (list.Length >= 3)
            {
                year = (int)list[2];
            }
            if (list.Length >= 4)
            {
                fullHour = (int)list[3];
            }

            if (day < 1 || day > Datetime.MAX_DAYS)
            {
                return "Error. Day must be within [1, " + Datetime.MAX_DAYS + "]";
            }
            if (month < 1 || month > Datetime.MonthNames.Length)
            {
                return "Error. Month must be within [1, " + Datetime.MonthNames.Length + "]";
            }
            if (year < 1)
            {
                return "Error. Year must be a positive integer.";
            }

            // how can I invoke SetTimeOfDayCommand?

            Datetime.Day = day;
            Datetime.Month = month;
            Datetime.Year = year;
            // set new time?

            return string.Format("New date and time: {0}", Datetime.FullDatetimeString());
        }
    }
}