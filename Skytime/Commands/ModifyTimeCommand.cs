﻿using IW.Console;
using UnityEngine;

namespace IW.Skytime
{
    public class ModifyTimeCommand : Command
    {
        public override string GetCommand()
        {
            return "modtime";
        }

        public override Property[] GetProperties()
        {
            return new Property[]
            {
                new Property { Name = "Minutes", Type = typeof(int) }
            };
        }

        protected override string Logic(params object[] list)
        {
            int minutes = (int)list[0];

            Datetime.ModifyMinutes(minutes);

            return string.Format("New date and time: {0}", Datetime.FullDatetimeString());
        }
    }
}