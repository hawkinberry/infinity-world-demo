﻿using UnityEngine;

namespace IW.Skytime
{
    public class TimeComponent : MonoBehaviour
    {
        void FixedUpdate()
        {
            Datetime.Update(Time.fixedDeltaTime);
        }
    }
}