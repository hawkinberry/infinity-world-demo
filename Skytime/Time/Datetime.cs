﻿using IW.Utility;
using System;
using UnityEngine;

namespace IW.Skytime
{
    public struct DateTime_s
    {
        public float Seconds;
        public int Minutes;
        public int Hour;
        public int Day;
        public int Month;
        public int Year;
        public int Age;
    };

    public static class Datetime
    {
        #region Constants
        private const float scale = 20;
        public const float MAX_SECONDS = 60;
        public const int MAX_MINUTES = 60;
        public const int MAX_HOURS = 24;
        public const int MAX_DAYS = 30;
        private const float dayPercent = 7.000f / 12.000f;
        public const int shiftHour = -6;
        #endregion

        #region Time Variables
        public static float Seconds = 0;
        public static int Minutes = 0;
        public static int Hour = 8;
        public static int Day = 1;
        public static int Month = 1;
        public static int Year = 1;
        public static int Age = 1;
        #endregion

        public static string[] MonthNames =
        {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        };

        #region Events
        public static event TimeEventHandler OnDayTime;
        public static event TimeEventHandler OnNightTime;
        public static event TimeEventHandler OnDayChange;

        public static event HourEventHandler OnHourChange;

        public static EventElement[] OnHour = new EventElement[]
        {
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement(),
            new EventElement()
        };
        #endregion

        private static bool wasDay = false;
        private static int previousDay = Day;

        public static bool IsDay
        {
            get
            {
                return PercentOfDay() < dayPercent;
            }
        }

        public static bool IsNight
        {
            get
            {
                return PercentOfDay() >= dayPercent;
            }
        }

        #region Stringify Time
        public static string MonthString()
        {
            return MonthNames[Month - 1];
        }

        public static string DateString()
        {
            return string.Format("{0} {1}, {2}", MonthString(), Day.ToString(), Year.ToString());
        }

        public static string TimeString()
        {
            return string.Format("{0}:{1}", Hour.ToString(), Minutes.ToString("D2"));
        }

        public static string TimeString(float fractionHour)
        {
            int hour = (int)System.Math.Floor(fractionHour);
            int minutes = (int)((fractionHour - hour) * MAX_MINUTES);
            return string.Format("{0}:{1}", hour.ToString(), minutes.ToString("D2"));
        }

        public static string FullDatetimeString()
        {
            return string.Format("{0} {1}", DateString(), TimeString());
        }
        #endregion

        public static void Update(float deltaTime)
        {
            Seconds += deltaTime * scale;

            if (Seconds >= MAX_SECONDS)
            {
                Seconds -= MAX_SECONDS;
                ModifyMinutes(1);
            }

            if (IsDay && !wasDay)
            {
                OnDayTime?.Invoke();
                wasDay = true;
            }
            if (IsNight && wasDay)
            {
                OnNightTime?.Invoke();
                wasDay = false;
            }
            if (GetDayOfYear() != previousDay)
            {
                OnDayChange?.Invoke();
                previousDay = GetDayOfYear();
            }
        }

        public static void ModifyMinutes(int minutes)
        {
            if (minutes < 0) return;

            Minutes += minutes;

            while (Minutes >= MAX_MINUTES)
            {
                if (Minutes >= MAX_MINUTES)
                {
                    Minutes -= MAX_MINUTES;
                    Hour++;
                    if (Hour < MAX_HOURS)
                    {
                        OnHour[Hour]?.Invoke(null, EventArgs.Empty);
                        OnHourChange?.Invoke(Hour);
                    }
                    else if (Hour == MAX_HOURS)
                    {
                        OnHour[0]?.Invoke(null, EventArgs.Empty);
                        OnHourChange?.Invoke(0);
                    }
                }

                if (Hour >= MAX_HOURS)
                {
                    Hour -= MAX_HOURS;
                    Day++;
                }

                if (Day > MAX_DAYS)
                {
                    Day -= MAX_DAYS;
                    Month++;
                }

                if (Month > MonthNames.Length)
                {
                    Month -= MonthNames.Length;
                    Year++;
                }
            }
        }

        public static DateTime_s GetCurrentDate()
        {
            DateTime_s date = new DateTime_s
            {
                Seconds = Seconds,
                Minutes = Minutes,
                Day = Day,
                Month = Month,
                Year = Year,
                Age = Age
            };

            return date;
        }

        // Returns the total number of days in a year
        public static int MaxDaysInYear()
        {
            return MAX_DAYS * MonthNames.Length;
        }

        // Returns the day of the year (counts from 1)
        public static int GetDayOfYear()
        {
            return Day + ((Month - 1) * MAX_DAYS);
        }

        // Returns the day of the year (counts from 1)
        public static int GetDayOfYear(DateTime_s date)
        {
            return date.Day + ((date.Month - 1) * MAX_DAYS);
        }

        public static float PercentOfDay()
        {
            float secondPercent = (1.000f / MAX_HOURS) * (1.000f / MAX_MINUTES) * (Seconds / MAX_SECONDS);
            float minutePercent = (1.000f / MAX_HOURS) * (Minutes / (float)MAX_MINUTES);

            return (ShiftHour() / (float)MAX_HOURS) + minutePercent + secondPercent;
        }

        private static int ShiftHour()
        {
            int newHour = Hour + shiftHour;
            if (newHour < 0)
            {
                newHour += MAX_HOURS;
            }
            return newHour;
        }

        public delegate void TimeEventHandler();
        public delegate void HourEventHandler(int hour);
    }
}