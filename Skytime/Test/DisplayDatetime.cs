﻿using UnityEngine;
using UnityEngine.UI;

namespace IW.Skytime
{
    public class DisplayDatetime : MonoBehaviour
    {
        [SerializeField]
        private Text display;

        void Update()
        {
            display.text = Datetime.FullDatetimeString();
        }
    }
}