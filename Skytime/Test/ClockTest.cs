﻿using System;
using UnityEngine;

namespace IW.Skytime.Test
{
    public class ClockTest : MonoBehaviour
    {
        [SerializeField]
        private AudioSource ding;

        private void Awake()
        {
            Datetime.OnHourChange += EveryHour;
            //Datetime.OnHour[0] += Hour0;
            //Datetime.OnHour[1] += Hour1;
            //Datetime.OnHour[2] += Hour2;
            //Datetime.OnHour[3] += Hour3;
            //Datetime.OnHour[4] += Hour4;
            //Datetime.OnHour[5] += Hour5;
            //Datetime.OnHour[6] += Hour6;
            //Datetime.OnHour[7] += Hour7;
            //Datetime.OnHour[8] += Hour8;
            //Datetime.OnHour[9] += Hour9;
            //Datetime.OnHour[10] += Hour10;
            //Datetime.OnHour[11] += Hour11;
            //Datetime.OnHour[12] += Hour12;
            //Datetime.OnHour[13] += Hour13;
            //Datetime.OnHour[14] += Hour14;
            //Datetime.OnHour[15] += Hour15;
            //Datetime.OnHour[16] += Hour16;
            //Datetime.OnHour[17] += Hour17;
            //Datetime.OnHour[18] += Hour18;
            //Datetime.OnHour[19] += Hour19;
            //Datetime.OnHour[20] += Hour20;
            //Datetime.OnHour[21] += Hour21;
            //Datetime.OnHour[22] += Hour22;
            //Datetime.OnHour[23] += Hour23;
        }

        private void OnDestroy()
        {
            Datetime.OnHourChange -= EveryHour;
            //Datetime.OnHour[0] -= Hour0;
            //Datetime.OnHour[1] -= Hour1;
            //Datetime.OnHour[2] -= Hour2;
            //Datetime.OnHour[3] -= Hour3;
            //Datetime.OnHour[4] -= Hour4;
            //Datetime.OnHour[5] -= Hour5;
            //Datetime.OnHour[6] -= Hour6;
            //Datetime.OnHour[7] -= Hour7;
            //Datetime.OnHour[8] -= Hour8;
            //Datetime.OnHour[9] -= Hour9;
            //Datetime.OnHour[10] -= Hour10;
            //Datetime.OnHour[11] -= Hour11;
            //Datetime.OnHour[12] -= Hour12;
            //Datetime.OnHour[13] -= Hour13;
            //Datetime.OnHour[14] -= Hour14;
            //Datetime.OnHour[15] -= Hour15;
            //Datetime.OnHour[16] -= Hour16;
            //Datetime.OnHour[17] -= Hour17;
            //Datetime.OnHour[18] -= Hour18;
            //Datetime.OnHour[19] -= Hour19;
            //Datetime.OnHour[20] -= Hour20;
            //Datetime.OnHour[21] -= Hour21;
            //Datetime.OnHour[22] -= Hour22;
            //Datetime.OnHour[23] -= Hour23;
        }

        #region Event Handlers
        private void EveryHour(int hour)
        {
            Debug.Log(string.Format("Ding! Hour {0}", hour));
            ding.Play();
        }

        private void Hour0(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 0");
            ding.Play();
        }
        private void Hour1(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 1");
            ding.Play();
        }
        private void Hour2(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 2");
            ding.Play();
        }
        private void Hour3(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 3");
            ding.Play();
        }
        private void Hour4(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 4");
            ding.Play();
        }
        private void Hour5(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 5");
            ding.Play();
        }
        private void Hour6(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 6");
            ding.Play();
        }
        private void Hour7(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 7");
            ding.Play();
        }
        private void Hour8(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 8");
            ding.Play();
        }
        private void Hour9(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 9");
            ding.Play();
        }
        private void Hour10(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 10");
            ding.Play();
        }
        private void Hour11(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 11");
            ding.Play();
        }
        private void Hour12(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 12");
            ding.Play();
        }
        private void Hour13(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 13");
            ding.Play();
        }
        private void Hour14(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 14");
            ding.Play();
        }
        private void Hour15(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 15");
            ding.Play();
        }
        private void Hour16(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 16");
            ding.Play();
        }
        private void Hour17(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 17");
            ding.Play();
        }
        private void Hour18(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 18");
            ding.Play();
        }
        private void Hour19(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 19");
            ding.Play();
        }
        private void Hour20(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 20");
            ding.Play();
        }
        private void Hour21(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 21");
            ding.Play();
        }
        private void Hour22(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 22");
            ding.Play();
        }
        private void Hour23(object sender, EventArgs eventArgs)
        {
            Debug.Log("Ding! Hour 23");
            ding.Play();
        }
        #endregion
    }
}