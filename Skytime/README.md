# System: DateTime

---

This page is currently a design doc. As the system matures, it can be archived and converted into an implementation guide.

---
## World Time
It is important that certain physical mechanics align with the real world, such as the composition of a Day (24 h, 60 m, 60 s). Changing these root systems would probably throw off our basic, learned instincts. How quickly game time passes is less important than the relationship between the units.

- Seconds (60)
- Minutes (60)
- Hour (24)
- Day
- Month
- Year
- Age

This isolated world time manager is a system that can be loosely linked to other systems. For example, the rising and setting of a Sun object can be tied to the passing of time. However, due to the principle of independence, if the passing of time were ever removed, the Sun would still exist (but not move).

## Time Events
The passing of time triggers certain predefined events. These events can be subscribed to by systems that wish to be affected by the passing of world time.

```csharp
DateTime.OnDay
DateTime.OnNight
DateTime.OnHour
```

## Design Guidance: Modifying Time
Either through the dev console or by some in-game modifier, the current DateTime can be set or advanced. Doing so has implications on how objects that perceive time respond to the change in time.

### Advance Time
When time is advanced (at a given rate), all timed objects must account for the passage of time. For example, a character that was walking from one town to the next should progress forward a distance proportional to their rate of travel in that time that was skipped. 

This need certainly has broad implications. In the context of timed events, advanced time is achieved by invoking all the events that would have been invoked in that same amount of time, only as fast as possible. Performance would limit the practicality of this, and in actuality, advancing time would likely require an amount of simulation (or abstracting) the events (such as increasing the coarseness of their evaluation).

### Set Time
When time is set (to either a DateTime in the past or in the future), it is not considered an advance of time. Instead, time has discretely changed. In this case, all time-based events should be designed as checks rather than triggers. That is, certain events should not be triggered by a specific time event but by a specific time condition.

For example, a character who finishes work at 16:00 does not "miss the whistle" if the time is suddenly set to 17:00 (the 16:00 event was never triggered). Instead, that character should be performing a time check and see that the time is now (suddenly) past 16:00.
