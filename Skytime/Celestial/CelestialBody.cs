﻿using UnityEngine;

namespace IW.Skytime
{
    public static class Constants
    {
        public const float RAD_TO_DEG = 180.0f / Mathf.PI;
        public const float DEG_TO_RAD = Mathf.PI / 180.0f;
        public const float TWO_PI = 2.0f * Mathf.PI;
    }

    public abstract class MotionModel : ScriptableObject
    {
        public abstract float CalculateElevation(float latitude, DateTime_s date);

        public abstract float CalculatePosition(float percentOfDay);
    }

    public class CelestialBody : MonoBehaviour
    {
        public Light Light;
        public MotionModel Motion;

        public AnimationCurve IntensityCurve;

        private float Intensity;

        // Used to track whether this object should be up during the day. Probably temporary.
        bool isDayObject = true;
        public bool IsDayObject
        {
            set { isDayObject = value; }
        }

        private float localLatitude = 0.0f;
        public float LocalLatitude
        {
            set { localLatitude = value; }
        }

        void Awake()
        {
            Datetime.OnDayTime += DayTime;
            Datetime.OnNightTime += NightTime;
            Datetime.OnDayChange += DayChange;

            Intensity = Light.intensity;
        }

        void Start()
        {
            SetElevation();
        }

        void Update()
        {
            Advance(Datetime.PercentOfDay());
        }

        public void Advance(float percentOfDay)
        {
            // Calculate new position based on time of day
            float newRotation = Motion.CalculatePosition(percentOfDay);

            // Set rotation to new position
            Light.transform.localRotation = Quaternion.Euler(new Vector3(newRotation, Light.transform.localRotation.y, Light.transform.localRotation.z));

            // Calculate and set new intensity based on time of day
            Light.intensity = IntensityCurve.Evaluate(percentOfDay) * Intensity;
        }

        // Set the daily elevation of the celestial body as it appears at solar noon from the observer's latitude on earth
        private void SetElevation()
        {
            float newElevation = Motion.CalculateElevation(localLatitude, Datetime.GetCurrentDate()); // Elevation will oscillate between max and min (solstice)
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, 90.0f - newElevation)); // transform.rotation is measured from zenith
        }

        private void DayChange()
        {
            SetElevation();
        }

        private void DayTime()
        {
            Light.shadows = (isDayObject? LightShadows.Soft : LightShadows.None);
        }
        private void NightTime()
        {
            Light.shadows = (isDayObject ? LightShadows.None : LightShadows.Soft);
        }
    }
}