﻿using UnityEngine;

namespace IW.Skytime
{
    [CreateAssetMenu(fileName = "New Celestial Model", menuName = "Celestial Model/Solar")]
    class SolarModel : MotionModel
    {
        public AnimationCurve RotationCurve;

        private float CalculateDeclination(DateTime_s date)
        {
            // int days is number of days since Jan 01
            int days = Datetime.GetDayOfYear(date) - 1;

            // Formula for declincation d is based upon earth's obliquity of 23.5 degrees
            // d = A * cos[B * (N - 173)]
            // N = days since Jan 01
            // 173 is the day of the summer solstice, i.e. when the sun reaches max declination
            // A = approx 23.5
            // B = 360 / 365.25 = 0.98563 is the frequency of the oscillation (i.e., 1 rotation per 365.25 days)
            // http://mypages.iit.edu/~maslanka/SolarGeo.pdf

            int solstice = Datetime.MaxDaysInYear() / 2;
            float maxDeclination = 23.45f;
            float frequency = 360.0f / Datetime.MaxDaysInYear();
            float declination = (float)(maxDeclination * Mathf.Cos(Constants.DEG_TO_RAD * frequency * (days - solstice)));

            return declination;
        }

        public override float CalculateElevation(float latitude, DateTime_s date)
        {
            float offset = latitude - CalculateDeclination(date);
            // Subtract offset for Northern Hemisphere
            // Add offset for Southern Hemisphere
            // Note: I'm not exactly sure what is meant to happen at the equator (i.e., latitude = 0)
            return (90.0f + (latitude > 0 ? -1 * offset : offset));
        }

        public override float CalculatePosition(float percentOfDay)
        {
            return (360.0f * RotationCurve.Evaluate(percentOfDay));
        }

        // Calculate the time that the sun should reach its maximum elevation
        // int days is number of days since Jan 01
        public float CalculateSolarNoonInLocalTime(DateTime_s date)
        {
            // Solar noon is defined as the time that the sun is due-south (or north, in the Southern Hemisphere). This is defined to be 12:00 in solar time.
            // To calculate this time in local clock time, we subtract an hour offset based on the day of the year
            // This offset is defined as the "Equation of Time" http://www.powerfromthesun.net/Book/chapter03/chapter03.html
            // According to this, the sun is as "slow" as 14 minutes in February (i.e., solar noon occurs at 12:14)
            // and as "fast" as 16 minutes in November (i.e., solar noon occurs at 11:44)

            float x_rad = Constants.DEG_TO_RAD * (360.0f / Datetime.MaxDaysInYear()) * (Datetime.GetDayOfYear(date) - 1);
            float EOT_min = 0.258f * Mathf.Cos(x_rad) - 7.416f * Mathf.Sin(2 * x_rad) - 9.228f * Mathf.Sin(2 * x_rad);
            float lct_hour = 12.0f - (EOT_min / Datetime.MAX_MINUTES);

            return lct_hour;
        }
    }
}
