﻿using UnityEngine;

namespace IW.Skytime
{
    [CreateAssetMenu(fileName = "New Celestial Model", menuName = "Celestial Model/Lunar")]
    class LunarModel : MotionModel
    {
        public AnimationCurve RotationCurve;

        private float CalculateDeclination(DateTime_s date)
        {
            // int days is number of days since Jan 01
            int days = Datetime.GetDayOfYear(date);

            // Based upon https://astronomy.stackexchange.com/questions/29932/how-to-calculate-declination-of-moon
            // modified as the Moon goes faster or slower over an anomalistic month where t is the number of days since 2000-01-01 12:00 TT (J2000.0)
            // (we'll just use our zero-day here)
            float u = days - 0.48f * Mathf.Sin(Constants.TWO_PI / 27.55455f * (days - 3.45f));

            // We can make a crude approximation with two sine waves: one for the Moon's travel eastward around the ecliptic over a tropical month
            float d1_deg = 23.45f * Mathf.Sin(Constants.TWO_PI / 27.32158f * (u - 10.75f));

            // and one for the Moon's travel north or south of the ecliptic over a draconic month
            float d2_deg = 5.1f * Mathf.Sin(Constants.TWO_PI / 27.21222f * (u - 20.15f));

            // Note: the u and t offsets above are near a 0° longitude crossing, an ascending node,
            // and an apogee in January 2000 but are tweaked to minimize RMS differences from
            // JPL HORIZONS lunar positions for 2000-2018.

            float d_deg = d1_deg + d2_deg;

            return d_deg;
        }

        public override float CalculateElevation(float latitude, DateTime_s date)
        {
            float offset = latitude - CalculateDeclination(date);
            // Subtract offset for Northern Hemisphere
            // Add offset for Southern Hemisphere
            // Note: I'm not exactly sure what is meant to happen at the equator (i.e., latitude = 0)
            return (90.0f + (latitude > 0 ? -1 * offset : offset));
        }

        public override float CalculatePosition(float percentOfDay)
        {
            return (360.0f * RotationCurve.Evaluate(percentOfDay));
        }
    }
}
