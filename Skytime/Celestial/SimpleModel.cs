﻿using UnityEngine;

namespace IW.Skytime
{
    [CreateAssetMenu(fileName = "New Celestial Model", menuName = "Celestial Model/Simple")]
    public class SimpleModel : MotionModel
    {
        public AnimationCurve RotationCurve;

        public override float CalculateElevation(float latitude, DateTime_s date)
        {
            return 90.0f;
        }

        public override float CalculatePosition(float percentOfDay)
        {
            return (360.0f * RotationCurve.Evaluate(percentOfDay));
        }
    }
}